package resources;

import static org.testng.Assert.assertTrue;

import org.sikuli.script.Match;
import org.sikuli.script.Screen;

public class ImageVerify {
    Screen s = new Screen();

	
	public void imageVerify()
	{
	 Match img1 = s.exists(ImagePath.imgpath+"lobby.png");
     assertTrue(img1 != null);
}
	public void lobbyMenuIV()
	{
		Match img1 = s.exists(ImagePath.imgpath+"lobbyMenuIV.png");
	     assertTrue(img1 != null);
	}
	public void missionsIV()
	{
		Match img1 = s.exists(ImagePath.imgpath+"missionIV.png");
	     assertTrue(img1 != null);
	}
	public void AddCashIV()
	{
		Match img1 = s.exists(ImagePath.imgpath+"AddCashIV.png");
	     assertTrue(img1 != null);
	}

}
