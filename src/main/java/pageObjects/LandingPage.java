package pageObjects;

import static org.testng.Assert.assertTrue;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;

import org.sikuli.script.FindFailed;
import org.sikuli.script.Match;
import org.sikuli.script.Pattern;
import org.sikuli.script.Screen;

import resources.ImagePath;

public class LandingPage {
 
	Screen s = new Screen();

	
//	public void melbOk() throws InterruptedException, FindFailed
//	{
//		  // s.wait(pause,20);
//        s.doubleClick(ImagePath.imgpath+"OK.png");
//        Thread.sleep(100);
//       
//        	
//	}
	
	public void EnterPhoneNumber() throws AWTException, InterruptedException, FindFailed
	{
	     // Select textbox
		   System.out.println( "path is" + ImagePath.imgpath+"Enter_No.png" );
	        Thread.sleep(1000);
	        s.click(ImagePath.imgpath+"Enter_No.png");
	     //tipe.sendKeys("9999999999");
	        Thread.sleep(100); // Thread.sleep throws InterruptedException	
	        Robot robot = new Robot();
	        robot.keyPress(KeyEvent.VK_2);
	        robot.delay(100);
	        robot.keyPress(KeyEvent.VK_2);
	        robot.delay(100);
	        robot.keyPress(KeyEvent.VK_2);
	        robot.delay(100);
	        robot.keyPress(KeyEvent.VK_2);
	        robot.delay(100);
	        robot.keyPress(KeyEvent.VK_2);
	        robot.delay(100);
	        robot.keyPress(KeyEvent.VK_2);
	        robot.delay(100);
	        robot.keyPress(KeyEvent.VK_2);
	        robot.delay(100);
	        robot.keyPress(KeyEvent.VK_2);
	        robot.delay(100);
	        robot.keyPress(KeyEvent.VK_2);
	        robot.delay(100);
	        robot.keyPress(KeyEvent.VK_2);
	        robot.delay(100);
	        Thread.sleep(200);
	        robot.keyPress(KeyEvent.VK_ENTER);  
	        Thread.sleep(200);
	}
	
	public void EnterOTP() throws AWTException, FindFailed
	{
            Pattern OTP = new Pattern("/Users/rishabh/Desktop/OTP.png");
            s.click(OTP);
            Robot robot = new Robot();

		    robot.keyPress(KeyEvent.VK_1);
	        robot.delay(100);
	        robot.keyPress(KeyEvent.VK_1);
	        robot.delay(100);
	        robot.keyPress(KeyEvent.VK_1);
	        robot.delay(100);
	        robot.keyPress(KeyEvent.VK_1);
	        robot.delay(100);
	}
	
	public void Login() throws FindFailed, InterruptedException
	{
        Pattern Login = new Pattern(ImagePath.imgpath+"Login.png");	
        Pattern Login2 = new Pattern("/Users/rishabh/Desktop/Login2.png");
        s.click(Login);
        Thread.sleep(200);
        s.click(Login2);
        Thread.sleep(200);
	}
	public void lobbyMenu() throws FindFailed, InterruptedException
	{
		s.click(ImagePath.imgpath+"lobbyMenu.png");
        Thread.sleep(100);
	}
	public void TapOutSide() throws FindFailed, InterruptedException
	{
		s.click(ImagePath.imgpath+"TapOutSide.png");
        Thread.sleep(100);
	}
	public void missions() throws FindFailed, InterruptedException
	{
		s.click(ImagePath.imgpath+"missions.png");
        Thread.sleep(100);
	}
	
	public void backButtonMissions() throws FindFailed, InterruptedException
	{
		s.click(ImagePath.imgpath+"backButtonMissions.png");
        Thread.sleep(100);
	}
	public void TapOutSide02() throws FindFailed, InterruptedException
	{
		s.click(ImagePath.imgpath+"TapOutSide02.png");
        Thread.sleep(100);
	}
	public void AddCash() throws FindFailed, InterruptedException
	{
		s.click(ImagePath.imgpath+"AddCash.png");
        Thread.sleep(100);
	}
	public void AddCashBack() throws FindFailed, InterruptedException
	{
		s.click(ImagePath.imgpath+"AddCashBack.png");
        Thread.sleep(100);
	}
	
	
}
