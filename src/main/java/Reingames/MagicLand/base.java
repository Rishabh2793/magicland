package Reingames.MagicLand;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class base {
   public WebDriver driver;

public WebDriver intializeDriver() throws IOException
{

	Properties prop = new Properties();
	FileInputStream fis = new FileInputStream("../MagicLand/src/main/java/data.properties");
	prop.load(fis);
	String browserName= prop.getProperty("browser");
	
	if(browserName.equals("chrome"))
	{
        System.setProperty("webdriver.chrome.driver", "/Users/rishabh/Downloads/chromedriver");
        driver = new ChromeDriver();
        driver.get("https://playmagicrummy.com/build/webbuild/web-mobileLiveRishabh/index.html");
       //debug link 
       //driver.get("https://playmagicrummy.com/build/webbuild/web-mobileDebugForRishabh/index.html");
    
	}
	else if (browserName.equals("firefox"))
	{
		 System.setProperty("webdriver.chrome.driver", "/Users/rishabh/Downloads/chromedriver");
	        
	}
     driver.manage().window().maximize();
     driver.manage().deleteAllCookies();
     driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
     return driver;
	}
}
