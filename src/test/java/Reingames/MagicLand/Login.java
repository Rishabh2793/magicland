package Reingames.MagicLand;

import java.awt.AWTException;
import java.io.IOException;

import org.python.modules.thread.thread;
import org.sikuli.script.FindFailed;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pageObjects.LandingPage;
import resources.ImageVerify;

public class Login extends base {
	
	LandingPage lp = new LandingPage();
	ImageVerify iv = new ImageVerify();

	@BeforeSuite
	public void launchBrowser() throws IOException, InterruptedException
	{
		driver = intializeDriver();
		Thread.sleep(5000);// will wait till loading
	}
	
	@BeforeTest
	public void login() throws FindFailed, InterruptedException, AWTException
	{
		//lp.melbOk();
		//lp.melbOk();
		lp.EnterPhoneNumber();
		lp.EnterOTP();
		lp.Login();
		//lp.melbOk();
		Thread.sleep(3000);
	}
	
//	@AfterTest
//	public void closeBrowser()
//	{
//		driver.close();
//	}
	
	@Test
	public void Test010_loginPage() throws IOException, FindFailed, InterruptedException, AWTException
	{
		
		
		lp.TapOutSide02();
		Thread.sleep(2000);
		iv.imageVerify();
		
	}
	
	@Test
	public void Test030_lobbyMenu() throws InterruptedException, FindFailed
	{
		Thread.sleep(2000);
		lp.lobbyMenu();
		Thread.sleep(2000);
		iv.lobbyMenuIV();
		Thread.sleep(2000);
		lp.TapOutSide();
		
	}
	
	@Test
	public void Test020_missions() throws InterruptedException, FindFailed
	{
		Thread.sleep(2000);
		lp.missions();
		Thread.sleep(2000);
		iv.missionsIV();
		lp.backButtonMissions();
	}
	
	@Test
	public void Test040_AddCash() throws FindFailed, InterruptedException
	{
		Thread.sleep(2000);
		lp.AddCash();
		Thread.sleep(2000);
		iv.AddCashIV();
		Thread.sleep(2000);
		lp.AddCashBack();
	}
	
	
		
}
	
	
	


